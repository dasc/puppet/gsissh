define gsissh::matchblock (
    Array[Hash] $options,
    String $type,
    Variant[String,Array[String]] $pattern,
    Integer $order = 50,
) {
    include ::gsissh

    if $pattern =~ String {
        $stringified_pattern = $pattern
    } else {
        $stringified_pattern = join($pattern, ',')
    }

    concat::fragment { "matchblock ${name}":
	#target  => lookup(gsissh::sshd_config),
	target  => $::gsissh::sshd_config,
	content => epp("${module_name}/etc/gsissh/matchblock_fragment.epp", {
	    'type' => $type,
	    'pattern' => $stringified_pattern,
	    'options' => $options,
	    'invalid_options' => $::gsissh::invalid_options,
	}),
	order   => 200+$order,
    }
}
